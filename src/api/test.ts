import request, { CustomAxiosPromise } from "../service/axios";

export const test = (): CustomAxiosPromise<any[]> => {
  return request({
    url: "http://192.168.200.192:3001/mock/4991/spyy/detail/1/base-info",
    method: "get",
  });
};
