// 引入 router
import { createRouter, createWebHistory } from "vue-router";
import type { RouteRecordRaw } from "vue-router";

// 路由配置基本信息
const routes: RouteRecordRaw[] = [
  {
    path: "/",
    redirect: "/open-layers",
    children: [
      {
        name: "home",
        path: "/home",
        component: () => import("/@/views/home/index.vue"),
      },
      {
        name: "test",
        path: "/test",
        component: () => import("/@/views/test/index.vue"),
      },
      {
        name: "openLayers",
        path: "/open-layers",
        component: () => import("/@/views/openlayers/index.vue"),
      },
    ],
  },
];

const router = createRouter({
  routes: [...routes],
  history: createWebHistory(import.meta.env.VITE_BASE_URL),
  // history: createWebHistory(),
  // base: '',
});

export default router;
