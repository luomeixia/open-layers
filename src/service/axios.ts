// vite引入axios必须从dist文件夹里引入，否则打包之后线上环境会报错
import {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponseHeaders,
  InternalAxiosRequestConfig,
  RawAxiosResponseHeaders,
} from "axios";
import axios from "axios";
import { notification } from "ant-design-vue";

// 创建一个实例
const request: AxiosInstance = axios.create({
  baseURL: "http://192.168.211.11",
  timeout: 50000,
  headers: {
    "Content-Type": "application/json;charset=UTF-8;",
  },
});

export interface CustomAxiosResponse<T = any, D = any> {
  data: T;
  status: number;
  statusText: string;
  headers: RawAxiosResponseHeaders | AxiosResponseHeaders;
  config: InternalAxiosRequestConfig<D>;
  request?: any;
  message: string;
}

export type CustomAxiosPromise<T = any> = Promise<CustomAxiosResponse<T>>;

// 请求拦截
request.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // 发送请求之前的处理，比如token
    return config;
  },
  (error) => {
    // 请求错误时做的事
    return Promise.reject(error);
  }
);

// 响应拦截
request.interceptors.response.use(
  (response) => {
    // 响应成功处理
    return response.data;
  },
  (error) => {
    // 相应失败的处理，一般是状态码404 401之类的
    // 或者是message不是成功时的提示信息处理
    return Promise.reject(error);
  }
);

// 异常拦截处理器
const responseErrorHandler = (error: AxiosError) => {
  if (axios.isCancel(error)) {
    return Promise.reject(error);
  }
  if (error.message === "Network Error") {
    // 系统重定向
    // router.push("/login");
    return Promise.reject(error);
  }
  const { response } = error;
  if (response) {
    if (response.status === 401) {
      //   router.push("/login");
      return Promise.reject(error);
    }
  }
  notification.error({
    message: error.name,
    description: error.message,
  });
  return Promise.reject(error);
};

// 响应拦截
request.interceptors.response.use((response) => {
  // 响应成功
  return response.data;
}, responseErrorHandler);

export default request;
