import { defineComponent, onMounted, ref } from "vue";
import "ol/ol.css";
import { Feature, View, Map, Overlay } from "ol";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { XYZ, OSM, Vector as VectorSource } from "ol/source";
import { fromLonLat, transformExtent } from "ol/proj";
import { ScaleLine, defaults as defaultControls } from "ol/control";
import { Style, Fill, Stroke, Circle as sCircle, Icon, Text } from "ol/style";
import { Point, Polygon } from "ol/geom";
import iconImage from "/@/assets/empty-map-icon-3.png";
import GeoJSON from "ol/format/GeoJSON.js";

export default defineComponent({
  setup() {
    const map = ref<Map>();
    const mapList = ref<number[][]>([]);
    const shopPopup = ref<boolean>(false);
    const popupRef = ref<HTMLElement>();
    const popup = ref();
    /**
     * 初始化地图
     */
    const initMap = () => {
      map.value = new Map({
        target: "ol-map",
        layers: [
          new TileLayer({
            // 内置地图
            // source: new OSM(),
            // 高德地图
            source: new XYZ({
              // 高德瓦片，最大支持放大到20级，颜色偏灰绿色。
              // url: "http://webst0{1-4}.is.autonavi.com/appmaptile?style=7&x={x}&y={y}&z={z}",
              // 高德瓦片，最大支持放大到18级，最常用的样式
              // url: "http://webrd01.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8",
              // url: "http://wprd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}",
              url: "http://wprd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=8&x={x}&y={y}&z={z}",
            }),
          }),
        ],
        view: new View({
          // 视图的初始中心，经纬度
          // center: [104.06, 30.67],
          // projection: "EPSG:4326",
          // zoom: 4,
          center: fromLonLat([104.06, 30.67]),
          zoom: 4,
          maxZoom: 17,
          minZoom: 4,
          // extent: [60, 0, 180, 70],
          extent: transformExtent([60, 0, 180, 70], "EPSG:4326", "EPSG:3857"),
        }),
        controls: defaultControls({
          zoom: false,
          rotate: false,
          attribution: true,
        }),
      });
      setMarker();
      addOverlay();
      singleClickMarker();
      map.value.on("dblclick", () => {
        shopPopup.value = false;
      });

      // 调整地图背景色
      // map.value.on("postcompose", function (e) {
      //   var canvas = document.querySelector("canvas");
      //   var ctx = canvas!.getContext("2d");
      //   canvas!.style.backgroundColor = "#58a";
      //   ctx!.globalCompositeOperation = "color";
      //   ctx!.fillStyle = "#58a";
      //   ctx!.strokeStyle = "#58a";
      //   ctx!.fillRect(0, 0, canvas!.width, canvas!.height);
      // });
    };
    /**
     * 设置弹框
     */
    const addOverlay = () => {
      // 创建Overlay
      popup.value = new Overlay({
        element: popupRef.value,
        positioning: "bottom-center",
        stopEvent: false,
        offset: [0, -50],
      });
      map.value!.addOverlay(popup.value);
    };
    /**
     * 设置点
     */
    const setMarker = () => {
      const iconMarkers = mapList.value.map((marker) => {
        const style = new Style({
          text: new Text({
            offsetY: -8,
            offsetX: 0,
            placement: "point",
            // 位置
            textAlign: "center",
            // 基准线
            textBaseline: "middle",
            // 文字样式
            font: "normal 13px sans-serif",
            // 文本内容
            text: "1",
            // 文字颜色
            fill: new Fill({
              color: "#ffcc33",
            }),
          }),
          image: new Icon({
            src: iconImage,
            scale: 0.8,
          }),
        });
        const feature = new Feature({
          geometry: new Point(fromLonLat(marker)),
        });
        feature.setStyle(style);
        return feature;
      });
      const markerLayer = new VectorLayer({
        source: new VectorSource({
          features: iconMarkers,
        }),
      });
      map.value!.addLayer(markerLayer);
    };
    onMounted(() => {
      mapList.value = [
        [103.77, 29.57],
        [100.77, 29.57],
        [98.77, 27.57],
        [114.3, 30.59],
      ];
      initMap();
    });
    /**
     * 放大缩小
     */
    const onScale = (type: "+" | "-") => {
      const view = map.value?.getView()!;
      let curZoom = view.getZoom() as number;
      curZoom = type === "+" ? curZoom + 1 : curZoom - 1;
      view.setZoom(curZoom);
    };
    /**
     * 单击标注
     */
    const singleClickMarker = () => {
      map.value!.on("singleclick", (e) => {
        // 判断是否点击在点上
        const feature = map.value!.forEachFeatureAtPixel(
          e.pixel,
          (feature) => feature
        );
        if (feature) {
          shopPopup.value = true;
          // 设置弹窗位置
          const coordinates = feature!.getGeometry()!.getCoordinates();
          popup.value.setPosition(coordinates);
        } else {
          shopPopup.value = false;
        }
      });
    };
    /**
     * 回到地图中心
     */
    const goCenter = () => {
      map.value?.getView().setCenter(fromLonLat([104, 30.6]));
      map.value?.getView().setZoom(10);
    };
    return {
      shopPopup,
      popupRef,
      onScale,
      goCenter,
    };
  },
});
