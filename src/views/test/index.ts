import { defineComponent, ref } from "vue";
import { test } from "/@/api/test";

export default defineComponent({
  setup(props, ctx) {
    const showText = ref<boolean>(false);
    const getParentDom = () => {
      return document.querySelector("#container") as HTMLElement;
    };
    const click = async () => {
      showText.value = !showText.value;
      const { data, message } = await test();
    };
    return {
      showText,
      getParentDom,
      click,
    };
  },
});
