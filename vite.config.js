import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

export default defineConfig({
  plugins: [vue()],
  resolve: {
    // vite中别名必须以 / 开头否则不能正常引入
    alias: {
      "/@": resolve(__dirname, "src"),
    },
  },
  server: {
    host: "0.0.0.0", //ip地址
    port: 8888, //端口号
    hrm: true,
  },
});
